#! /bin/bash

#BSUB -P CSC314
#BSUB -W 2:00
#BSUB -nnodes 1


echo "PETSC_ARCH=${PETSC_ARCH}"

#RS allocation
#rs_rnum=6;      #opt  --rs_per_host  (-r)
#rs_cpu=7;       #opt: --cpu_per_rs   (-c) default 1
#rs_gpu=1;       #opt: --gpu_per_rs   (-g) default 0
#rs_tasks=1;     #opt: --tasks_per_rs (-a) total tasks (-p), or MPI ranks
#nodes=1;        #opt: --nrs          (-n) number of resource sets, all cores
#rs_cores=$nodes*$rs_rnum

#additonal options
#--bind                 (-b)    Binding of tasks within a resource set. Can be none, rs, or packed:#,.          Rec.:packed:7
#--latency_priority     (-l)    Latency Priority. Can currently be cpu-cpu or gpu-cpu,cpu-mem,cpu-cpu,gpu-gpu.  Rec.:GPU-GPU
#--launch_distribution  (-d)    How tasks are started on resource sets  packed.                                 Rec.:packed
export OMP_NUM_THREADS=1;
#sed -n 130,400p rsconfigs_test.tab > rsconfigs.tab
let SIZE=$1;
#Read file with possible configurations
filename="rsconfigs.tab";
newconf="rsconfigs_pruned.tab";
n=0;
declare -a arr=()
#store in an array all fields
while read line; do
arr[n]=$line;
let "n++"
done < $filename

n=0
for line in "${arr[@]}"
  do
    export $line
    let "n++"
    output="configs/rsperf_${SIZE}_${n}.log"
    fsize=$(stat --format=%s $output)
    #echo $fsize
    if [ $fsize -gt 14000 ]; then
        echo $line >> $newconf
    fi
  done