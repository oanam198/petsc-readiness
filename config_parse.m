clear all
close all

%output files path
fname = 'C:\Users\oana_\gitcode\petsc01_21\src\ts\tutorials\perftest\figs';

%separate configurations into "good" and "bad" and slect their ID
goodconfs=[120, 122, 124, 126,130, 132, 133, 136, 137, 140, 141, 144, 145, 146, ...
    148, 149, 150, 164, 166, 168, 170, 171, 174, 175, 178, 179, 182, 183, 186,...
    187, 196, 200, 201];
confsf=114:202;
confsbad=confsf;
goodtemp=goodconfs-113;
confsbad(goodtemp)=[];

rs_tasksarr=[];
rs_cpuarr=[];
rs_gpuarr=[];
rs_rnumarr=[];
nodesarr=[];

confidall=1:202; confidgood=1:202;
confidgood(confsbad)=[];

%read and parse configs file for heatmap
readfile=importdata('rsconfigs_pruned.tab');
for i=1:length(confidgood)
    k=confidgood(i);
    linei=strsplit(readfile{k});
    for j=1:5
        eval([linei{j} ';']);
    end
    rs_tasksarr=[rs_tasksarr rs_tasks];
    rs_cpuarr=[rs_cpuarr rs_cpu];
    rs_gpuarr=[rs_gpuarr rs_gpu];
    rs_rnumarr=[rs_rnumarr rs_rnum];
    %nodesarr=[nodesarr nodes];
    optsp=strsplit(linei{6},'=');
    bindtypearr{i}=optsp{2};
    optsp=strsplit(linei{7},'=');
    packdistarr{i}=optsp{2};
    optsp=strsplit(linei{8},'=');
    latencyarr{i}=optsp{2};
end

nodesarr=rs_rnumarr;
Mheat=NaN(19,146);
ropt1=find(rs_rnumarr==1);
Mheat(1,ropt1)=1;
ropt2=find(rs_rnumarr==2);
Mheat(2,ropt2)=1;
ropt3=find(rs_rnumarr==3);
Mheat(3,ropt3)=1;
ropt4=find(rs_rnumarr==6);
Mheat(4,ropt4)=1;

aopt1=find(rs_tasksarr==1);
Mheat(5,aopt1)=1;
aopt2=find(rs_tasksarr==2);
Mheat(6,aopt2)=1;
aopt3=find(rs_tasksarr==3);
Mheat(7,aopt3)=1;
aopt4=find(rs_tasksarr==6);
Mheat(8,aopt4)=1;

copt1=find(rs_cpuarr==1);
Mheat(9,copt1)=1;
copt2=find(rs_cpuarr==2);
Mheat(10,copt2)=1;
copt3=find(rs_cpuarr==4);
Mheat(11,copt3)=1;
copt4=find(rs_cpuarr==21);
Mheat(12,copt4)=1;
copt5=find(rs_cpuarr==42);
Mheat(13,copt5)=1;

gopt1=find(rs_gpuarr==1);
Mheat(14,gopt1)=1;
gopt2=find(rs_gpuarr==3);
Mheat(15,gopt2)=1;
gopt3=find(rs_gpuarr==6);
Mheat(16,gopt3)=1;

bopt1=strcmp('rs',bindtypearr);
Mheat(17,bopt1)=1;
bopt2=strcmp('packed:1',bindtypearr);
Mheat(18,bopt2)=1;
bopt3=strcmp('packed:7',bindtypearr);
Mheat(19,bopt3)=1;

msize=[100000 1000000 10000000 100000000];
nc=202;
mean_s1=zeros(4,nc);
var_s1=zeros(4,nc);
mean_s2=zeros(4,nc);
var_s2=zeros(4,nc);
mean_s3=zeros(4,nc);
var_s3=zeros(4,nc);
mean_s4=zeros(4,nc);
var_s4=zeros(4,nc);

%%%%this parser can be replaced with a slimmer version, to be updated
for i=1:nc
    if (i==20)
        continue
    end
    fformat=sprintf('databatchall/stats/stats_%d_%d.log',msize(1),i);
    varlog1=importdata(fformat);
    op=varlog1.textdata;
    mean_s1(1,i)=str2num(op{1,3});
    var_s1(1,i)=str2num(op{1,5});
    mean_s1(2,i)=str2num(op{2,3});
    var_s1(2,i)=str2num(op{2,5});
    mean_s1(3,i)=str2num(op{3,3});
    var_s1(3,i)=str2num(op{3,5});
    mean_s1(4,i)=str2num(op{4,3});
    var_s1(4,i)=str2num(op{4,5});
    fformat=sprintf('databatchall/stats/stats_%d_%d.log',msize(2),i);
    varlog1=importdata(fformat);
    op=varlog1.textdata;
    mean_s2(1,i)=str2num(op{1,3});
    var_s2(1,i)=str2num(op{1,5});
    mean_s2(2,i)=str2num(op{2,3});
    var_s2(2,i)=str2num(op{2,5});
    mean_s2(3,i)=str2num(op{3,3});
    var_s2(3,i)=str2num(op{3,5});
    mean_s2(4,i)=str2num(op{4,3});
    var_s2(4,i)=str2num(op{4,5});
    fformat=sprintf('databatchall/stats/stats_%d_%d.log',msize(3),i);
    varlog1=importdata(fformat);
    op=varlog1.textdata;
    mean_s3(1,i)=str2num(op{1,3});
    var_s3(1,i)=str2num(op{1,5});
    mean_s3(2,i)=str2num(op{2,3});
    var_s3(2,i)=str2num(op{2,5});
    mean_s3(3,i)=str2num(op{3,3});
    var_s3(3,i)=str2num(op{3,5});
    mean_s3(4,i)=str2num(op{4,3});
    var_s3(4,i)=str2num(op{4,5});
    fformat=sprintf('databatchall/stats/stats_%d_%d.log',msize(4),i);
    varlog1=importdata(fformat);
    op=varlog1.textdata;
    mean_s4(1,i)=str2num(op{1,3});
    var_s4(1,i)=str2num(op{1,5});
    mean_s4(2,i)=str2num(op{2,3});
    var_s4(2,i)=str2num(op{2,5});
    mean_s4(3,i)=str2num(op{3,3});
    var_s4(3,i)=str2num(op{3,5});
    mean_s4(4,i)=str2num(op{4,3});
    var_s4(4,i)=str2num(op{4,5});
end

header1=op{1,1};
header2=op{2,1};
header3=op{3,1};
header4=op{4,1};

%Need to set to NaN for plotting the configurations that are "bad"
mean_s1trim=mean_s1;
mean_s1trim(:,confsbad)=NaN;
var_s1trim=var_s1;
var_s1trim(:,confsbad)=NaN;

mean_s2trim=mean_s2;
mean_s2trim(:,confsbad)=NaN;
var_s2trim=var_s2;
var_s2trim(:,confsbad)=NaN;

mean_s3trim=mean_s3;
mean_s3trim(:,confsbad)=NaN;
var_s3trim=var_s3;
var_s3trim(:,confsbad)=NaN;

mean_s4trim=mean_s4;
mean_s4trim(:,confsbad)=NaN;
mean_s4heat=mean_s4;
mean_s4heat(:,confsbad)=[];
var_s4trim=var_s4;
var_s4trim(:,confsbad)=NaN;

%select 1 configuration to assess noise only
id=200;
mean_ms=[mean_s1trim(1,id),mean_s2trim(1,id),mean_s3trim(1,id),mean_s4trim(1,id)];
std_ms=[var_s1trim(1,id),var_s2trim(1,id),var_s3trim(1,id),var_s4trim(1,id)];

%this plot doesn't look nice, the alternative is MATLAB's errorbar however
%it has the same issues, see separate file for noise 
figure(99)
semilogy(mean_ms,'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','k')
hold on
semilogy(mean_ms+sqrt(std_ms),'^','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','r')
semilogy(mean_ms-sqrt(std_ms),'v','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','r')

xlabel('Config ID')
ylabel('Time in s')
title('Noise variation')
legend('size 10^6')
grid on
filename=[char(op{1,1}),'_stdmsize'];
saveas(gcf, fullfile(fname,filename), 'epsc');
saveas(gcf, fullfile(fname,filename), 'png');

%generate heatmap figure
figure(77)
heattemp=repmat(mean_s4heat(1,:),19,1);
heatdat=heattemp.*Mheat;
xvalues = {'-r 1','-r 2','-r 3', '-r 6','-a 1','-a 2','-a 3', '-a 6',...
    '-c 1','-c 2','-c 4', '-c 21','-c 42','-g 1','-g 3', '-g 6',...
    '-b rs','-b packed:1', '-b packed:7'};
yvalues = split(num2str(1:146));
h = heatmap(yvalues,xvalues,heatdat);
%set(gca,'YTickLabel',[]);

h.Title = 'Configuration paramter performance';
h.XLabel = 'Configuration IDs';
h.YLabel = 'jsrun Parameters';
filename='heatmap1e8';
saveas(gcf, fullfile(fname,filename), 'epsc');
saveas(gcf, fullfile(fname,filename), 'png');
saveas(gcf, fullfile(fname,filename), 'fig');

%%%%%%%%%Figures per operation as parsed by logfile
figure(1)
semilogy(mean_s2(1,:),'s','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','red')
hold on
semilogy(mean_s2trim(1,:),'s','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','black')
xlabel('Config ID')
ylabel('Time in s')
ylim([1e-5 1e-4])
xlim([1 202])
title(op{1,1})
legend('size 10^6')
grid on
filename=[char(op{1,1}),'_trim2'];
saveas(gcf, fullfile(fname,filename), 'epsc');
saveas(gcf, fullfile(fname,filename), 'png');

figure(2)
semilogy(mean_s2(3,:),'s','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','red')
hold on
semilogy(mean_s2trim(3,:),'s','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','black')
xlabel('Config ID')
ylabel('Time in s')
ylim([1e-5 1e-4])
xlim([1 202])
title(op{3,1})
legend('size 10^6')
grid on
filename=[char(op{3,1}),'_trim2'];
saveas(gcf, fullfile(fname,filename), 'epsc');
saveas(gcf, fullfile(fname,filename), 'png');

figure(3)
semilogy(mean_s1trim(1,:),'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','k')
hold on
semilogy(mean_s2trim(1,:),'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','r')
semilogy(mean_s3trim(1,:),'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','g')
semilogy(mean_s4trim(1,:),'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','b')
xlabel('Config ID')
ylabel('Time in s')
ylim([1e-5 1e-2])
xlim([1 202])
title(op{1,1})
legend('10^5', '10^6','10^7','10^8')
grid on
filename=[char(op{1,1}),'_multi'];
saveas(gcf, fullfile(fname,filename), 'epsc');
saveas(gcf, fullfile(fname,filename), 'png');

figure(4)
semilogy(mean_s1trim(3,:),'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','k')
hold on
semilogy(mean_s2trim(3,:),'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','r')
semilogy(mean_s3trim(3,:),'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','g')
semilogy(mean_s4trim(3,:),'*','LineWidth',2,'MarkerSize',5,...
    'MarkerEdgeColor','b')
xlabel('Config ID')
ylabel('Time in s')
ylim([1e-5 1e-2])
xlim([1 202])
title(op{3,1})
legend('10^5','10^6','10^7','10^8')
grid on
filename=[char(op{3,1}),'_multi'];
saveas(gcf, fullfile(fname,filename), 'epsc');
saveas(gcf, fullfile(fname,filename), 'png');



